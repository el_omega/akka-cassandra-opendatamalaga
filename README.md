## Akka and Cassandra integration

This is a project for training the akka and cassandra connector. For this use case, we will download the data of the position GPS of the bus
of the city of Malaga and then save it into Cassandra.
The url for the open data of Malaga is : https://datosabiertos.malaga.eu/dataset 

The project contains three actors:

1. BusScanActor : Downloads the data of the open data platform of the position GPS of the buses.
2. BusReadActor : Read the data from Cassandra.
3. BusWriterActor : Write the data in Cassandra.
	
---

**Data model**

1. codBus : Identifier of the vehicle.
2. codLinea : Identifier of the line of the bus.
3. sentido : Direction of the bus.
4. lon : Position of longitude.
5. lat : Position of latitude.
6. codParIni : Code of the start bus stop.
7. last_update : The las update of the csv.
