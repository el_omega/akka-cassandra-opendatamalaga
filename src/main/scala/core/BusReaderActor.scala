package core

import domain.Bus
import akka.actor.Actor
import com.datastax.driver.core.Cluster
import com.datastax.driver.core.{BoundStatement, Row}
import com.datastax.driver.core.querybuilder.QueryBuilder
import core.BusReaderActor.{FindAll}


object BusReaderActor {
  case class FindAll(maximum: Int = 10)
}

class BusReaderActor(cluster: Cluster) extends Actor{

  val session = cluster.connect(Keyspaces.akkaCassandra)

  import scala.collection.JavaConversions._
  import cassandra.resultset._
  import context.dispatcher
  import akka.pattern.pipe

  def buildBus(r: Row): Bus = {
    val codbus = r.getInt("codbus")
    val sentido = r.getInt("sentido")
    val codlinea = r.getFloat("codlinea")
    val codparlini = r.getInt("codparlini")
    val last_update = r.getString("last_update")
    val lat = r.getFloat("lat")
    val lon = r.getFloat("lon")
    Bus(codbus, sentido, codlinea, codparlini, last_update, lat, lon)
  }

  def receive: Receive = {
    case FindAll(maximum)  =>
      val query = QueryBuilder.select().all().from(Keyspaces.akkaCassandra, "busmalagagps").limit(maximum)
      session.executeAsync(query) map(_.all().map(buildBus).toVector) pipeTo sender

  }
}
