package core

import java.text.SimpleDateFormat

import akka.actor.{Actor, ActorRef}
import domain.Bus

trait TweetMarshaller {
  type Tweets = Vector[Bus]

  implicit object TweetUnmarshaller extends Unmarshaller[Tweets] {

    val dateFormat = new SimpleDateFormat("EEE MMM d HH:mm:ss Z yyyy")

    def mkTweet(status: JsValue): Deserialized[Bus] = {
      val json = status.asJsObject
      (json.fields.get("id_str"), json.fields.get("text"), json.fields.get("created_at"), json.fields.get("user")) match {
        case (Some(JsString(id)), Some(JsString(text)), Some(JsString(createdAt)), Some(JsObject(user))) =>
          user.get("id_str") match {
            case Some(JsString(userId)) => Right(Tweet(id, userId, text, dateFormat.parse(createdAt)))
            case _                      => Left(MalformedContent("Bad tweet JSON"))
          }
        case _                          => Left(MalformedContent("Bad status JSON"))
      }
    }

    def apply(entity: HttpEntity): Deserialized[Tweets] = {
      val json = JsonParser(entity.asString).asJsObject
      json.fields.get("statuses") match {
        case Some(JsArray(statuses)) => Right(statuses.map(t => mkTweet(t).right.get))
        case _                       => Left(MalformedContent("statuses missing"))
      }
    }
  }

}

class BusScanActor(busWriter : ActorRef) extends Actor{

  val queryUrl : String = "http://datosabiertos.malaga.eu/recursos/transporte/EMT/EMTLineasUbicaciones/lineasyubicaciones.csv"

  override def receive: Receive = ???
}
