package core

import akka.actor.Actor
import com.datastax.driver.core.Cluster
import domain.Bus


class BusWriterActor (cluster: Cluster) extends Actor {


  val session = cluster.connect(Keyspaces.akkaCassandra)
  val preparedStatement = session.prepare(
    "INSERT INTO busmalagagps(codbus, sentido, codlinea, codparlini, last_update, lat, lon) VALUES (?, ?, ?, ?, ?, ? , ?);")

  def saveBus(bus: Bus): Unit =
    session.executeAsync(preparedStatement.bind(bus.codbus, bus.sentido, bus.codLinea, bus.codParIni
    , bus.lastUpdate, bus.lat, bus.lon))

  def receive: Receive = {
    case buses: List[Bus] => buses foreach saveBus
    case bus:  Bus       => saveBus(bus)
  }

}
