package object core {

  private[core] object Keyspaces {
    val akkaCassandra = "busmalaga"
  }

  private[core] object ColumnFamilies {
    val bus = "busmalagagps"
  }

}