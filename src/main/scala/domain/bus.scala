package domain

import java.util.Date

case class CodBus(codbus: Int) extends AnyVal
object CodBus {
  implicit def toCodBus(codbus: Int): CodBus = CodBus(codbus)
}

case class Sentido(sentido: Int) extends AnyVal
object Sentido {
  implicit def toSentido(sentido: Int): Sentido = Sentido(sentido)
}

case class CodLinea(codLinea: Float) extends AnyVal
object CodLinea {
  implicit def toCodLinea(codLinea: Float): CodLinea = CodLinea(codLinea)
}

case class CodParIni(codParIni: Int) extends AnyVal
object CodParIni {
  implicit def toCodParIni(codParIni: Int): CodParIni = CodParIni(codParIni)
}

case class LastUpdate(lastUpdate: String) extends AnyVal
object LastUpdate {
  implicit def toLastUpdate(lastUpdate: String): LastUpdate = LastUpdate(lastUpdate)
}

case class Lat(lat: Float) extends AnyVal
object Lat {
  implicit def toLat(lat: Float): Lat = Lat(lat)
}

case class Lon(lon: Float) extends AnyVal
object Lon {
  implicit def  toLat(lon: Float): Lon = Lon(lon)
}


case class Bus(codbus: CodBus, sentido: Sentido , codLinea: CodLinea, codParIni: CodParIni,lastUpdate: LastUpdate, lat: Lat, lon: Lon )