// Package Information

name := "example" // change to project name
organization := "com.databricks" // change to your org
version := "0.1-SNAPSHOT"
scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.13",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.13" % Test,
  "org.xerial.snappy"       % "snappy-java"           % "1.1.1.3",
  "org.scala-lang"          % "scala-reflect"         % "2.10.4",
  "com.datastax.cassandra"  % "cassandra-driver-core" % "2.1.1"  exclude("org.xerial.snappy", "snappy-java")
)





